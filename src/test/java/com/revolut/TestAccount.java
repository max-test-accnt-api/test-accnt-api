package com.revolut;

import com.revolut.account.api.*;
import com.revolut.account.api.impl.InMemoryAccountManager;
import org.junit.*;
import static org.junit.Assert.*;

public class TestAccount {
    private static AccountsManager am;

    @BeforeClass
    public static void setup() throws Exception {
        am = InMemoryAccountManager.instance();
        am.create("john doe", 100);
        am.create("bill smith", 200);

    }

    @Test(expected = AccountNotFoundException.class)
    public void testNonExistingAccount() throws AccountNotFoundException {
        Account account = am.get(10);
        assert false;
    }

    @Test
    public void testCreateAccount() throws AccountNotFoundException {
        String holderName = "mr. x";
        Account account = am.create(holderName, 100);
        Account testAccount = am.get(account.getId());
        assertEquals(account, testAccount);
    }

    @Test (expected = AccountNotFoundException.class)
    public void testAccNotFoundTransfer () throws Exception {
        am.transfer(10, 20, 100);
        assert false;
    }

    @Test (expected = TransactionException.class)
    public void testAmountTooHigh () throws Exception {
        am.transfer(1, 2, 1000);
        assert false;
    }

    @Test
    public void testTransfer() throws Exception {
        Transaction t = am.transfer(1, 2, 10);
        assertEquals(t.getId(), 1);
        assertEquals(t.getAmount(), 10);
        assertEquals(t.getSrcAccountId(), 1);
        assertEquals(t.getDstAccountId(), 2);
    }
}
