package com.revolut.account.api;

public class TransactionException extends Exception {
    private int srcAccountId;
    private int dstAccountId;
    private long amount;

    public TransactionException(int srcAccountId, int dstAccountId, long amount, String msg) {
        super(msg);
        this.srcAccountId = srcAccountId;
        this.dstAccountId = dstAccountId;
        this.amount = amount;
    }

    public int getSrcAccountId() {
        return srcAccountId;
    }

    public int getDstAccountId() {
        return dstAccountId;
    }

    public long getAmount() {
        return amount;
    }
}
