package com.revolut.account.api;

import java.util.Collection;

public interface AccountsManager {
    Collection<Account> listAccounts();
    Account get(int id) throws AccountNotFoundException;
    Account create(String holderName, long amount);
    Transaction transfer(int srcAccountId, int dstAccountId, long amount) throws AccountNotFoundException, TransactionException;
}
