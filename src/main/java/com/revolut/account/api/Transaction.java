package com.revolut.account.api;

public class Transaction {
    private int id;
    private int srcAccountId;
    private int dstAccountId;
    private long amount;

    public Transaction(int id, int srcAccountId, int dstAccountId, long amount) {
        this.id = id;
        this.srcAccountId = srcAccountId;
        this.dstAccountId = dstAccountId;
        this.amount = amount;
    }

    public Transaction() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSrcAccountId() {
        return srcAccountId;
    }

    public void setSrcAccountId(int srcAccountId) {
        this.srcAccountId = srcAccountId;
    }

    public int getDstAccountId() {
        return dstAccountId;
    }

    public void setDstAccountId(int dstAccountId) {
        this.dstAccountId = dstAccountId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Transaction)) {
            return false;
        }
        Transaction t = (Transaction)obj;
        return t.getSrcAccountId() == getSrcAccountId() && t.getDstAccountId() == getDstAccountId() && t.getId() == getId() && t.getAmount() == getAmount();
    }
}
