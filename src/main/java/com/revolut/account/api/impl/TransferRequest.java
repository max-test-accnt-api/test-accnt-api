package com.revolut.account.api.impl;

public class TransferRequest {
    private int srcAccountId;
    private int dstAccountId;
    private long amount;

    public TransferRequest() {
    }

    public int getSrcAccountId() {
        return srcAccountId;
    }

    public void setSrcAccountId(int srcAccountId) {
        this.srcAccountId = srcAccountId;
    }

    public int getDstAccountId() {
        return dstAccountId;
    }

    public void setDstAccountId(int dstAccountId) {
        this.dstAccountId = dstAccountId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
