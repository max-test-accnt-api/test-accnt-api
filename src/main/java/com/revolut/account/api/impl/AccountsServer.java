package com.revolut.account.api.impl;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class AccountsServer {
    private Server jettyServer;

    public AccountsServer(int port) {
        jettyServer = new Server(port);
    }

    public void start() throws Exception {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        jettyServer.setHandler(context);
        ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);
        jerseyServlet.setInitParameter("jersey.config.server.provider.classnames",
                AccountsController.class.getCanonicalName() + "," + AccountServiceExceptionHandler.class.getCanonicalName());
        jettyServer.start();
    }

    public void stop() throws Exception {
        jettyServer.stop();
    }
}
