package com.revolut.account.api.impl;

import com.revolut.account.api.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class InMemoryAccountManager implements AccountsManager {

    private static class AccountManagerContainer {
        private final static AccountsManager INSTANCE = new InMemoryAccountManager();
    }

    private AtomicInteger accountIdGenerator = new AtomicInteger();
    private AtomicInteger transactionIdGenerator = new AtomicInteger();

    private ConcurrentHashMap<Integer, Account> accounts = new ConcurrentHashMap<>();

    public static AccountsManager instance() {
        return AccountManagerContainer.INSTANCE;
    }

    public Collection<Account> listAccounts() {
        return new ArrayList<>(accounts.values());
    }

    public Account get(int id) throws AccountNotFoundException {
        if (!accounts.containsKey(id)) {
            throw new AccountNotFoundException(id);
        }

        return accounts.get(id);
    }

    public Account create(final String holderName, long amount) {
        Account account = new Account(accountIdGenerator.incrementAndGet(), amount, holderName);
        accounts.put(account.getId(), account);
        return account;
    }

    private Transaction commitTransaction(Account srcAccount, Account dstAccount, long amount) throws TransactionException {
        if (srcAccount.getAmount() < amount) {
            throw new TransactionException(srcAccount.getId(), dstAccount.getId(), amount, "Not anough money on src account");
        }
        srcAccount.setAmount( srcAccount.getAmount() - amount );
        dstAccount.setAmount( dstAccount.getAmount() + amount );
        return new Transaction(transactionIdGenerator.incrementAndGet(), srcAccount.getId(), dstAccount.getId(), amount);
    }

    public Transaction transfer(final int srcAccountId, final int dstAccountId, final long amount) throws AccountNotFoundException, TransactionException {
        if (amount <= 0) {
            throw new TransactionException(srcAccountId, dstAccountId, amount, "Amount must be positive");
        }

        if (srcAccountId == dstAccountId) {
            throw new TransactionException(srcAccountId, dstAccountId, amount, "Accounts must be different");
        }

        Account srcAccount = get(srcAccountId);
        Account dstAccount = get(dstAccountId);

        if (srcAccountId < dstAccountId) {
            synchronized (srcAccount) {
                synchronized (dstAccount) {
                    return commitTransaction(srcAccount, dstAccount, amount);
                }
            }
        } else {
            synchronized (dstAccount) {
                synchronized (srcAccount) {
                    return commitTransaction(srcAccount, dstAccount, amount);
                }
            }
        }
    }
}
