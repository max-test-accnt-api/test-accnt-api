package com.revolut.account.api.impl;

public class AccountCreationRequest {
    private String holderName;
    private long amount;

    public AccountCreationRequest(String holderName, long amount) {
        this.holderName = holderName;
        this.amount = amount;
    }

    public AccountCreationRequest() {
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }
}
