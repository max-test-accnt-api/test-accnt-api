package com.revolut.account.api.impl;

import com.revolut.account.api.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("account")
public class AccountsController {

    private AccountsManager accountsManager = InMemoryAccountManager.instance();

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Account getById(@PathParam("id") int id) throws AccountNotFoundException {
        return accountsManager.get(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Account createAccount(AccountCreationRequest accountCreationRequest) {
        return accountsManager.create(accountCreationRequest.getHolderName(), accountCreationRequest.getAmount());
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Transaction transfer(TransferRequest transferRequest) throws AccountNotFoundException, TransactionException {
        return accountsManager.transfer(transferRequest.getSrcAccountId(), transferRequest.getDstAccountId(), transferRequest.getAmount());
    }
}
