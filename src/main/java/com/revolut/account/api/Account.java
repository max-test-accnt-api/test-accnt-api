package com.revolut.account.api;

public class Account {
    private int id;
    private long amount;
    private String holderName;

    public Account(int id, long amount, String holderName) {
        this.id = id;
        this.amount = amount;
        this.holderName = holderName;
    }

    public Account() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Account)) {
            return false;
        }

        Account that = (Account)obj;
        return getId() == that.getId() && getAmount() == that.getAmount() && getHolderName() != null && getHolderName().equals(that.getHolderName());
    }
}
