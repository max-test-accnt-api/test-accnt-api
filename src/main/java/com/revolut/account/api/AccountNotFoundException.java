package com.revolut.account.api;

public class AccountNotFoundException extends Exception {
    private int accountId;

    public AccountNotFoundException(int accountId) {
        super("Account <" + accountId + "> doesn't exist");
        this.accountId = accountId;
    }

    public int getAccountId() {
        return accountId;
    }
}
