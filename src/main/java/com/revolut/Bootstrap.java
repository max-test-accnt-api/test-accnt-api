package com.revolut;

import com.revolut.account.api.impl.AccountsServer;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.Options;

public class Bootstrap {
    private final static String PORT = "port";

    public static void main(String[] args) throws Exception {
        int port = 8080;
        Options options = new Options();
        options.addOption(PORT, true, "");
        CommandLineParser parser = new GnuParser();
        try {
            CommandLine cmd = parser.parse(options, args);
            if (cmd.hasOption(PORT)) {
                port = Integer.parseInt( cmd.getOptionValue(PORT) );
            }
            AccountsServer srvr = new AccountsServer(port);
            srvr.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
